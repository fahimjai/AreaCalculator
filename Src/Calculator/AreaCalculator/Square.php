<?php
/**
 * Created by PhpStorm.
 * User: FAHIM
 * Date: 29-Mar-18
 * Time: 8:45 PM
 */

namespace Pondit\Calculator\AreaCalculator;


class Square
{
    public $arm;

    public function __construct($arm)
    {
        $this->arm = $arm;


    }

    public function square()
    {
        return $arm = $this->arm * $this->arm;

    }
}