<?php
/**
 * Created by PhpStorm.
 * User: FAHIM
 * Date: 29-Mar-18
 * Time: 7:32 PM
 */

namespace Pondit\Calculator\AreaCalculator;


class Triangle
{

    public $base=0;
    public $height=0;

    public function __construct($base, $height)
    {
        $this->base = $base;
        $this->height = $height;

    }

    public function triangle()
    {
        $area = $this->base * $this->height;
        return $area;
    }


}