<?php
/**
 * Created by PhpStorm.
 * User: FAHIM
 * Date: 29-Mar-18
 * Time: 7:32 PM
 */

namespace Pondit\Calculator\AreaCalculator;


class Circle
{
    public $radius = 0;
    public $pi = 0;

    public function __construct($radius, $pi)
    {
        $this->radius = $radius;
        $this->pi = $pi;
    }

    public function circle()
    {
        $area = $this->pi * $this->radius * $this->radius;
        return $area;
    }
}