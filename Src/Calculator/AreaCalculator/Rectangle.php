<?php
/**
 * Created by PhpStorm.
 * User: FAHIM
 * Date: 26-Mar-18
 * Time: 12:40 AM
 */

namespace Pondit\Calculator\AreaCalculator;


class Rectangle
{
    public $width =0;
    public $length=0;

    public function __construct($width, $length)
    {
        $this->width = $width;
        $this->length = $length;

    }

    public function rectangle()
    {
        $area = $this->width * $this->length;
        return $area;

    }

}

